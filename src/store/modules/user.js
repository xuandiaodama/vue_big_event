import { reqGetUserInfo } from '@/api/user'
// 用户模块
export default {
  state: {
    // 用户信息
    userInfo: {}
  },
  mutations: {
    // 设置用户信息
    setUserInfo (state, userInfo) {
      state.userInfo = userInfo
    }
  },
  actions: {
    async AgetUserInfo (context) {
      const { data } = await reqGetUserInfo()
      // 如果状态码是0 提交对应的mutation存储用户信息
      if (data.code === 0) {
        context.commit('setUserInfo', data.data)
      }
    }
  },
  getters: {
    avatarImgs (state) {
      // 用户如果有头像就显示头像,如果没有头像就显示昵称的首字母
      return state.userInfo.username ? state.userInfo.username.charAt(0).toUpperCase() : ''
    }
  },
  namespaced: true // 开启命名空间
}
