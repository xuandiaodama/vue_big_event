import { reqGetArticleList } from '@/api/article'

export default {
  // 开启命名空间
  namespaced: true,
  state () {
    return {
      // 文章分类的列表数据
      cateList: []
    }
  },
  mutations: {
    // 设置分类列表数据
    setCateList (state, list) {
      state.cateList = list
      console.log(state.cateList)
    }
  },
  getters: {},
  actions: {
    // 初始化文章分类
    async getCateList ({ commit }) {
      const res = await reqGetArticleList()
      console.log(res, 555)
      commit('setCateList', res.data.data)
    }
  }
}
