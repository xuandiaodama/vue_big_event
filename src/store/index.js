import Vue from 'vue'
import Vuex from 'vuex'
// 导入user模块
import user from './modules/user'
// 导入article模块
import article from './modules/article'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    // 挂载模块
    user,
    article
  }
})
