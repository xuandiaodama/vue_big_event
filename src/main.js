import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 按需导入ui组件
import '@/components/global'
import 'element-ui/lib/theme-chalk/index.css'
// 导入自定定义封装组件
import Bread from '@/components/Bread.vue'

import request from '@/utils/request'
Vue.component('Bread', Bread)

Vue.prototype.$http = request
// 关闭vue在console中的默认提示
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
