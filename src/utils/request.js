// 导入axios
import axios from 'axios'
// 导入组件库
import { Loading } from 'element-ui'

import router from '../router'
// 配置请求的基准URL地址
axios.defaults.baseURL = 'http://www.liulongbin.top:3008'

// axios设置请求拦截器,设置响应头token
let fullScreenLoading
axios.interceptors.request.use(
  (config) => {
    if (config.url.indexOf('/my') !== -1) {
      // 每次发送请求之前自动将在session中的token提取出来当做响应头header
      config.headers.Authorization = localStorage.getItem('token')
    }
    // 展示 loading 效果
    fullScreenLoading = Loading.service({
      text: '拼命加载中',
      fullscreen: true,
      background: 'rgba(0, 0, 0, 0.8)'
    })
    return config
  },
  (err) => {
    console.log(err)
  }
)

// 全局挂载 - 响应拦截器
axios.interceptors.response.use(
  (response) => {
    // 隐藏 loading 效果
    fullScreenLoading.close()
    return response
  },
  (error) => {
    // 响应失败会进入当前函数
    // 如果响应状态码是 401，则强制跳转到登录页面
    if (error.response.status === 401) {
      // 后台设定了 token 过期时间为 2小时左右
      // 我们在响应拦截器中统一处理token过期
      localStorage.removeItem('token')
      router.push('/login')
    }
    // 隐藏 loading 效果
    fullScreenLoading.close()
    // 返回一个带有拒绝原因的Promise对象
    return Promise.reject(error)
  }
)

// 导出axios
export default axios
