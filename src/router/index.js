import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    // 登录页
    path: '/login',
    name: 'Login',
    // 路由懒加载
    component: () => import('../views/Login/index.vue')
  },
  {
    // 注册页
    path: '/reg',
    name: 'Reg',
    component: () => import('../views/Reg/index.vue')
    // component: Home
  },
  {
    // 布局
    path: '/',
    name: 'Layout',
    component: () => import('../views/Layout/index.vue'),
    children: [
      {
        // 主页
        path: '/',
        name: 'Home',
        component: () => import('../views/Home/index.vue')
      },
      {
        // 文章分类
        path: 'artcategory',
        name: 'ArtCategory',
        component: () => import('../views/ArtCategory/index.vue')
      },
      {
        // 文章列表
        path: 'artlist',
        name: 'ArtList',
        component: () => import('../views/ArtList/index.vue')
      },
      {
        // 个人中心/基本资料
        path: 'userinfo',
        name: 'UserInfo',
        component: () => import('../views/UserInfo/index.vue')
      },
      {
        // 个人中心/更换头像
        path: 'changeavatar',
        name: 'ChangeAvatar',
        component: () => import('../views/ChangeAvatar/index.vue')
      },
      {
        // 个人中心/更换头像
        path: 'resetpwd',
        name: 'ResetPwd',
        component: () => import('../views/ResetPwd/index.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

// 配置路由守卫
// 定义白名单
const whiteList = ['/login', '/reg']
//  to=》去哪里   from=>从哪里来   next =>  放行   函数
router.beforeEach((to, from, next) => {
  // login +  reg 可以直接访问
  if (whiteList.includes(to.path)) {
    next()
  } else {
    if (localStorage.getItem('token')) {
      next()
    } else {
      next({ path: '/login' })
    }
  }
})

export default router
