import request from '@/utils/request'

// 封装登录API
export const reqLogin = (loginForm) => {
  return request({
    url: '/api/login',
    method: 'post',
    data: loginForm
  })
}
// 封装注册API
export const reqRegister = (regForm) => {
  return request({
    url: '/api/reg',
    method: 'post',
    data: regForm
  })
}

// 封装获取个人信息的接口
export const reqGetUserInfo = () => {
  return request({
    method: 'get',
    url: '/my/userinfo',
    headers: {
      Authorization: localStorage.getItem('token')
    }
  })
}
