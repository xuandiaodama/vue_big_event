import request from '@/utils/request'

export const reqGetArticleList = (obj) => {
  return request({
    method: 'get',
    url: '/my/cate/list'
  })
}

export const addCateApi = (obj) => {
  return request({
    url: '/my/cate/add',
    method: 'post',
    data: obj
  })
}
